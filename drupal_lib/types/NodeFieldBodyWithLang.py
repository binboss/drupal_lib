from dataclasses import dataclass, field

from .FieldValueWithLang import FieldValueWithLang

@dataclass
class NodeFieldBodyWithLang(FieldValueWithLang):
    """..."""
    processed: field(default_factory=str)
    format: str
    summary: str
    
    def __post_init__(self):
        self.format = "full_html"
        self.summary = ""