"""..."""
from dataclasses import dataclass
from typing import Any

@dataclass
class DefaultVal: # -> https://stackoverflow.com/users/2128545/mikeschneeberger
    """..."""
    val: Any

    def __len__(self):
        if hasattr(self.val, '__len__'):
            return len(self.val)

        return None