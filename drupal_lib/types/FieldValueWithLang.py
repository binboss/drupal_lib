from dataclasses import dataclass, field
from typing import Union, List, Dict

from .DataClass import DataClass

@dataclass
class FieldValueWithLang(DataClass):
    """..."""
    value: Union[str, int, bool, List, Dict]
    lang: field(default_factory=str)