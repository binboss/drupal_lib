"""..."""
from dataclasses import dataclass, field
from typing import List

from .Enum import *
from .DataClass import DataClass
from .NodeFieldBody import NodeFieldBody
from .FieldEntityTargetID import FieldEntityTargetID
from drupal_lib.types.FieldValue import FieldValue
from drupal_lib.types.FieldValueWithLang import FieldValueWithLang



@dataclass
class BaseNode(DataClass):
    """..."""

    # @dataclass
    # class _File(DataClass):
    #     """..."""
    #     url: str = None
    #     sha512: str = None
    #     size: int = None

    _links: field(default_factory=dict)
    title: List[FieldValueWithLang]
    type: List[FieldEntityTargetID]
    status: List[FieldValueWithLang]
    promote: List[FieldValueWithLang]
    sticky: List[FieldValueWithLang]
    default_langcode: List[FieldValueWithLang]
    # body: List[NodeFieldBody]