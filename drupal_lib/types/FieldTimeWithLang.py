from dataclasses import dataclass

from drupal_lib.types.FieldValueWithLang import FieldValueWithLang


@dataclass
class FieldTimeWithLang(FieldValueWithLang):
    """..."""
    format: str