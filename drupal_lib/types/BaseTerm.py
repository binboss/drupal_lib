"""..."""
from dataclasses import dataclass, field
from typing import List
from drupal_lib.types.FieldPathWithLang import FieldPathWithLang

from drupal_lib.types.FieldValue import FieldValue

from .Enum import *
from .DataClass import DataClass
from .FieldValueWithLang import FieldValueWithLang
from .FieldPath import FieldPath



@dataclass
class BaseTerm(DataClass):
    """..."""

    # @dataclass
    # class _File(DataClass):
    #     """..."""
    #     url: str = None
    #     sha512: str = None
    #     size: int = None

    _links: field(default_factory=dict)
    default_langcode: List[FieldValueWithLang]
    name: List[FieldValueWithLang]