from dataclasses import dataclass, field

from .FieldValue import FieldValue

@dataclass
class NodeFieldBody(FieldValue):
    """..."""
    processed: field(default_factory=str)
    format: str
    summary: str
    
    def __post_init__(self):
        format = "full_html"
        summary = ""