from dataclasses import dataclass
from typing import List, TypedDict

from .DataClass import DataClass
from .DefaultVal import DefaultVal
from .NoneRefersDefault import NoneRefersDefault

@dataclass
class FieldLink(NoneRefersDefault, DataClass):
    """..."""
    uri: str
    title: str
    options: List = DefaultVal([])

    def __post_init__(self):
        NoneRefersDefault.__post_init__(self)
        DataClass.__post_init__(self)