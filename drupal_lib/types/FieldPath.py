from dataclasses import dataclass, field
from typing import Union

from .DataClass import DataClass

@dataclass
class FieldPath(DataClass):
    """..."""
    alias: str
    pid: int

    def __post_init__(self):
        self.alias = None
        self.pid = None
        self.langcode = "zh-hans"
        self.lang = "zh-hans"