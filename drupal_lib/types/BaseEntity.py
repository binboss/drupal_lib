"""..."""
from dataclasses import dataclass, field
from typing import List

from .Enum import *
from .DataClass import DataClass
from drupal_lib.types.FieldValueWithLang import FieldValueWithLang



@dataclass
class BaseEntity(DataClass):
    """..."""

    # @dataclass
    # class _File(DataClass):
    #     """..."""
    #     url: str = None
    #     sha512: str = None
    #     size: int = None

    _links: field(default_factory=dict)
    default_langcode: List[FieldValueWithLang]
