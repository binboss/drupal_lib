from dataclasses import dataclass
from typing import Union

from drupal_lib.types.DataClass import DataClass

@dataclass
class FieldValue(DataClass):
    """..."""
    value: Union[str, int, bool]