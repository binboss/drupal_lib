"""..."""
from dataclasses import dataclass, fields
from .DataClass import DataClass
from .DefaultVal import DefaultVal

@dataclass
class NoneRefersDefault(DataClass): # -> https://stackoverflow.com/a/58081120/7728243
    """..."""
    def __post_init__(self):
        for field in fields(self):

            # if a field of this data class defines a default value of type
            # `DefaultVal`, then use its value in case the field after 
            # initialization has either not changed or is None.
            if isinstance(field.default, DefaultVal):
                field_val = getattr(self, field.name)
                if isinstance(field_val, DefaultVal) or field_val is None:
                    setattr(self, field.name, field.default.val)
