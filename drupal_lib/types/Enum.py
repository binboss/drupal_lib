"""字面量值"""
from typing import Optional, List

from .NodeFieldBody import NodeFieldBody as _NodeFieldBody

# from typing_extensions import Literal

NodeFieldBody = Optional[
    List[_NodeFieldBody]
]