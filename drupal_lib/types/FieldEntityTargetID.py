from dataclasses import dataclass
from typing import Union

from .DataClass import DataClass

@dataclass
class FieldEntityTargetID(DataClass):
    """..."""
    target_id: Union[str, int] = None