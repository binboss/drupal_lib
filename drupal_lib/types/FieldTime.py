from dataclasses import dataclass

from drupal_lib.types.FieldValue import FieldValue


@dataclass
class FieldTime(FieldValue):
    """..."""
    format: str
    lang: str = None