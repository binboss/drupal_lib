from sys import stdout
from typing import Callable, overload, List, NoReturn, Dict, Optional

import requests
from pathlib import Path
import json
from dataclasses import asdict

from drupal_lib.core import *
from drupal_lib.core.Config import *
from drupal_lib.types import *

_drupal_lib = Path.home().joinpath('.drupal_lib')
_drupal_lib.mkdir(parents=True, exist_ok=True)

class Auth:
    """..."""

    def debug_log(self, response: requests.Response) -> NoReturn:
        """打印错误日志, 便于分析调试"""
        r = response.request
        warn(f'[method status_code] {r.method} {response.status_code}')
        warn(f'[url] {response.url}')
        warn(f'[headers] {r.headers}')
        warn(f'[request body] {r.body}')
        warn(f'[response body] {response.text[:1024]}')

    def error_log_exit(self, response: requests.Response) -> NoReturn:
        """打印错误日志并退出"""
        self.debug_log(response)
        exit(-1)

    @overload
    def __init__(
            self,
            client_id: str,
            client_secret: str,
            name: str = 'drupal',
            
    ):
        """获取 access_token 登录

        :param name: (可选, 默认: drupal) 配置文件名称, 便于使用不同配置文件进行身份验证
        :param client_id
        :param client_secret
        """

    def __init__(
            self, name: str = 'drupal',
            access_token: str = None,
            client_id = '',
            client_secret = '',
            level: str = 'INFO',
    ):
        """登录验证

        :param name: (可选, 默认: drupal) 配置文件名称, 便于使用不同配置文件进行身份验证
        :param access_token:
        """

        self._name = _drupal_lib.joinpath(f'{name}.json')

        if level != 'INFO':
            logger.remove()
            logger.add(stdout, level=level)

        info(f'Config {self._name}')
        info(f'日志等级 {level}')

        self.session = requests.session()
        self.session.params.update(UNI_PARAMS)  # type:ignore
        self.session.headers.update(UNI_HEADERS)

        if access_token:
            debug('加载自定义 access_token')
            self.token = Token(
                access_token=access_token
            )
        
        elif self._name.exists():
            info(f'加载配置文件 {self._name}')
            self.token = Token(**json.load(self._name.open()))

        else:
            self.token: Optional[Token] = Token(
                access_token=access_token
            )

        self.session.headers.update({
            'Authorization': f'Bearer {self.token.access_token}'
        })

        self.client_id = client_id,
        self.client_secret = client_secret

    def _save(self) -> NoReturn:
        """保存配置文件"""
        info(f'保存配置文件: {self._name}')
        json.dump(asdict(self.token), self._name.open('w'))

    def _get_token(self, client_id: str = None, client_secret: str = None):
        """获取 token"""

        client_id = client_id if client_id is not None else self.client_id
        client_secret = client_secret if client_secret is not None else self.client_secret

        info('刷新 token ...')
        payload = {'grant_type': 'client_credentials', 'client_id': client_id, 'client_secret': client_secret}
        payload_tuples = [(k, v) for k, v in payload.items()]
        response = self.session.post(
            API_HOST + OAUTH_TOKEN,
            data=payload_tuples
        )
        if response.status_code == 200:
            self.token = Token(**response.json())
            self.session.headers.update({
                'Authorization': f'Bearer {self.token.access_token}'
            })
            self._save()
        else:
            error('获取 token 失败 ~')
            self.debug_log(response)
            # error_log_exit(response)

    def request(self, method: str, url: str,
                params: Dict = None, headers: Dict = None, data=None,
                files: object = None, verify: bool = None, body: Dict = None) -> requests.Response:
        """统一请求方法"""

        # 删除值为None的键
        if body is not None:
            body = {k: v for k, v in body.items() if v is not None}

        if data is not None and isinstance(data, dict):
            data = {k: v for k, v in data.items() if v is not None}

        for i in range(3):
            response = self.session.request(method=method, url=url, params=params,
                                            data=data, headers=headers, files=files,
                                            verify=verify, json=body)
            status_code = response.status_code
            info(
                f'{response.request.method} {response.url} {status_code} {response.headers.get("Content-Length", 0)}'
            )
            if status_code == 403:
                self._get_token()
                continue

            return response

        info(f'重试3次仍旧失败~')
        self.error_log_exit(response)

    def get(self, path: str, host: str = API_HOST, params: dict = None, headers: dict = None,
            verify: bool = None) -> requests.Response:
        """..."""
        return self.request(method='GET', url=host + path, params=params,
                            headers=headers, verify=verify)

    def post(self, path: str, host: str = API_HOST, params: dict = None, headers: dict = None, data: dict = None,
             files=None, verify: bool = None, body: dict = None) -> requests.Response:
        """..."""
        return self.request(method='POST', url=host + path, params=params, data=data,
                            headers=headers, files=files, verify=verify, body=body)

    def patch(self, path: str, host: str = API_HOST, params: dict = None, headers: dict = None, data: dict = None,
             files=None, verify: bool = None, body: dict = None) -> requests.Response:
        """..."""
        return self.request(method='PATCH', url=host + path, params=params, data=data,
                            headers=headers, files=files, verify=verify, body=body)

    def delete(self, path: str, host: str = API_HOST, params: dict = None, headers: dict = None,
            verify: bool = None) -> requests.Response:
        """..."""
        return self.request(method='DELETE', url=host + path, params=params,
                            headers=headers, verify=verify)