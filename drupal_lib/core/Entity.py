from typing import Generic
from drupal_lib.core import *
from drupal_lib.core.Config import *
from drupal_lib.request import *
from drupal_lib.response import *
from drupal_lib.types import *
from drupal_lib.types.DataClass import DataType


class Entity(BaseDrupalLib):

    def _core_get_entity(self, entity_type: str, request: GetEntityRequest, response_type: Generic[DataType]):
        response = self._get(f'{ENTITY}/{entity_type}/{request.entity_id}')
        result = self._result(response, response_type)
        return result

    def _core_create_entity(self, entity_type: str, body: CreateEntityRequest, response_type: Generic[DataType]):
        response = self._post(f'{ENTITY}/{entity_type}', body=body)
        return self._result(response, response_type, status_code = [200, 201])


    async def create_paragraph(self, entity_type: str, title: str, body: str = '', **kwargs):
        pass

    async def update_entity(self):
        pass


    async def patch_entity(self):
        pass
