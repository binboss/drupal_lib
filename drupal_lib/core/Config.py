"""..."""

import os

# 主机
API_HOST = os.environ.get('API_HOST', 'http://192.168.120.2:8080')

# 路径
NODE = '/node'
ENTITY = '/entity'
TERM = '/taxonomy/term'
PARAGRAPH = '/entity/paragraph'

OAUTH_TOKEN = '/oauth/token'

# 参数

UNI_PARAMS = {'_format': 'hal_json'}
UNI_HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36',
    'Content-Type': 'application/hal+json'
}