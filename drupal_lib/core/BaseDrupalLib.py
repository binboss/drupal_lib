import json
import subprocess
import traceback

from dataclasses import asdict
from typing import Dict, Optional, Union, List, Generic

import requests

from drupal_lib.core import *
from drupal_lib.core.Config import *
from drupal_lib.request import *
from drupal_lib.response import *
from drupal_lib.types import *
from drupal_lib.types.DataClass import DataType


class BaseDrupalLib:
    """..."""


    def __init__(self, auth: Optional[Auth] = None) -> None:
        super().__init__()
        self._auth: Auth = auth or Auth()

    def _get(self, path: str, host: str = API_HOST, params: Union[DataType, Dict] = None) -> requests.Response:
        """统一处理数据类型"""
        if params is None:
            params = {}
        elif isinstance(params, DataClass):
            params = asdict(params)

        return self._auth.get(path=path, host=host, params=params)

    def _post(self, path: str, host: str = API_HOST, body: Union[DataType, Dict] = None) -> requests.Response:
        """统一处理数据类型"""
        if body is None:
            body = {}
        elif isinstance(body, DataClass):
            body = asdict(body)

        return self._auth.post(path=path, host=host, body=body)

    def _patch(self, path: str, host: str = API_HOST, body: Union[DataType, Dict] = None) -> requests.Response:
        """统一处理数据类型"""
        if body is None:
            body = {}
        elif isinstance(body, DataClass):
            body = asdict(body)

        return self._auth.patch(path=path, host=host, body=body)

    def _delete(self, path: str, host: str = API_HOST, params: Union[DataType, Dict] = None) -> requests.Response:
        """统一处理数据类型"""
        if params is None:
            params = {}
        elif isinstance(params, DataClass):
            params = asdict(params)

        return self._auth.delete(path=path, host=host, params=params)

    def _result(self, response: requests.Response,
                dcls: Generic[DataType],
                status_code: Union[List, int] = 200) -> Union[Null, DataType]:
        """统一处理响应
        :param response:
        :param dcls:
        :param status_code:
        :return:
        """
        if isinstance(status_code, int):
            status_code = [status_code]
        if response.status_code in status_code:
            text = response.text
            if not text.startswith('{'):
                return dcls()
            try:
                return dcls(**json.loads(text))
            except TypeError:
                self._auth.debug_log(response)
                traceback.print_exc()
        warn(f'{response.status_code} {response.text}')
        return Null(response)