from typing import Dict, Generic
from drupal_lib.core import *
from drupal_lib.core.Config import *
from drupal_lib.request import *
from drupal_lib.response import *
from drupal_lib.types import *
from drupal_lib.types.DataClass import DataType


class Term(BaseDrupalLib):

    def _core_get_term(self, request: GetTermRequest, response_type: Generic[DataType]):
        response = self._get(f'{TERM}/{request.tid}')
        result = self._result(response, response_type)
        return result

    def _core_create_term(self, body: CreateTermRequest, response_type: Generic[DataType]):
        response = self._post(f'{TERM}', body=body)
        return self._result(response, response_type, status_code = [200, 201])

    def _core_patch_term(self, tid: int, body: Dict, response_type: Generic[DataType]):
        response = self._patch(f'{TERM}/{tid}', body=body)
        return self._result(response, response_type, status_code = [200, 201])

    def _core_delete_term(self, request: DeleteTermRequest, response_type: Generic[DataType]):
        response = self._delete(f'{TERM}/{request.tid}')
        result = self._result(response, response_type, status_code = 204)
        return result

    async def update_entity(self):
        pass


    async def patch_entity(self):
        pass
