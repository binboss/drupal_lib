from .Node import Node
from .Entity import Entity
from .Term import Term

class Core(
    Node,
    Entity,
    Term
):
    """..."""