from typing import Generic
from drupal_lib.core import *
from drupal_lib.core.Config import *
from drupal_lib.request import *
from drupal_lib.response import *
from drupal_lib.types import *
from drupal_lib.types.DataClass import DataType


class Node(BaseDrupalLib):

    def _core_get_node(self, request: GetNodeRequest, node_type: Generic[DataType]):
        response = self._get(f'{NODE}/{request.node_id}')
        result = self._result(response, node_type)
        return result

    def _core_create_node(self, body: CreateNodeRequest, response_type: Generic[DataType]):
        response = self._post(NODE, body=body)
        return self._result(response, response_type, status_code = [200, 201])

    async def create_node(self, node_type: str, title: str, body: str = '', **kwargs):
        data = {
            "_links": {
                "type": {
                    "href": f"{self.endpoint}/rest/type/node/{node_type}"
                }
            },
            "_embedded": {
                "http://192.168.120.2:8080/rest/relation/node/downloads/field_paid_download_link": [
                    {
                        "_links": {
                            "self": {
                                "href": ""
                            },
                            "type": {
                                "href": "http://192.168.120.2:8080/rest/type/paragraph/paid_download_link"
                            }
                        },
                        "uuid": [
                            {
                                "value": "9d72055a-efb5-4465-ac70-c06ff0af2cd5"
                            }
                        ],
                        "target_revision_id": 19705
                    }
                ]
            },
            "type": [
                {
                    "target_id": node_type
                }
            ],
            "status": [
                {
                    "value": True,
                    "lang": "zh-hans"
                }
            ],
            "title": [
                {
                    "value": title,
                    "lang": "zh-hans"
                }
            ],
            "promote": [
                {
                    "value": kwargs.pop('promote', False),
                    "lang": "zh-hans"
                }
            ],
            "sticky": [
                {
                    "value": kwargs.pop('sticky', False),
                    "lang": "zh-hans"
                }
            ],
            "default_langcode": [
                {
                    "value":  kwargs.pop('sticky', True),
                    "lang": "zh-hans"
                }
            ],
            "body": [
                {
                    "value": body,
                    "format": kwargs.pop('bodh_format', 'full_html'),
                    "summary": "",
                    "lang": "zh-hans"
                }
            ],
            "field_remark": [
                {
                    "value": "解压密码: cnroms.com@f5995b912b"
                }
            ],
            "field_roles": [
                {
                    "target_id": "premium"
                }
            ],
            "field_version": [
                {
                    "value": "11"
                }
            ],
            "field_tags": [
                {
                    "target_id": 18397
                }
            ]
        }


    async def create_paragraph(self, node_type: str, title: str, body: str = '', **kwargs):
        pass

    async def update_node(self):
        pass


    async def patch_node(self):
        pass
