"""..."""
from dataclasses import dataclass
from typing import List
from drupal_lib.types import *
from .GetTermResponse import GetTermResponse

@dataclass
class CreateTermResponse(GetTermResponse):
    """..."""
    revision_created: List[FieldTime]