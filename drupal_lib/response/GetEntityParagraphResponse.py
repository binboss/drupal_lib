"""..."""
from dataclasses import dataclass, field
from typing import List
from drupal_lib.types import *
from .GetEntityResponse import GetEntityResponse

@dataclass
class GetEntityParagraphResponse(GetEntityResponse):
    """..."""
    status: List[FieldValueWithLang]
    type: List[FieldEntityTargetID]
    behavior_settings: List[FieldValue]