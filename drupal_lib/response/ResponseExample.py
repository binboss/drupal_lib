def get_node_response():
    return {
        "_links": {
            "self": {
                "href": "http://192.168.120.2:8080/node/13857?_format=hal_json"
            },
            "type": {
                "href": "http://192.168.120.2:8080/rest/type/node/downloads"
            },
            "http://192.168.120.2:8080/rest/relation/node/downloads/revision_uid": [
                {
                    "href": "http://192.168.120.2:8080/user/1?_format=hal_json"
                }
            ],
            "http://192.168.120.2:8080/rest/relation/node/downloads/uid": [
                {
                    "href": "http://192.168.120.2:8080/user/1?_format=hal_json",
                    "lang": "zh-hans"
                }
            ],
            "http://192.168.120.2:8080/rest/relation/node/downloads/field_free_download_link": [
                {
                    "href": "http://192.168.120.2:8080/entity/paragraph/13303?_format=hal_json"
                }
            ],
            "http://192.168.120.2:8080/rest/relation/node/downloads/field_paid_download_link": [
                {
                    "href": "http://192.168.120.2:8080/entity/paragraph/13304?_format=hal_json"
                }
            ],
            "http://192.168.120.2:8080/rest/relation/node/downloads/field_tags": [
                {
                    "href": "http://192.168.120.2:8080/tags/SM-G9910?_format=hal_json",
                    "lang": "zh-hans"
                }
            ]
        },
        "nid": [
            {
                "value": 13857
            }
        ],
        "uuid": [
            {
                "value": "36f69797-1fb8-4f5a-82d5-34ae03fbe3c5"
            }
        ],
        "vid": [
            {
                "value": 19585
            }
        ],
        "langcode": [
            {
                "value": "zh-hans",
                "lang": "zh-hans"
            }
        ],
        "type": [
            {
                "target_id": "downloads"
            }
        ],
        "revision_timestamp": [
            {
                "value": "2021-09-28T19:48:36+00:00",
                "format": "Y-m-d\\TH:i:sP"
            }
        ],
        "_embedded": {
            "http://192.168.120.2:8080/rest/relation/node/downloads/revision_uid": [
                {
                    "_links": {
                        "self": {
                            "href": "http://192.168.120.2:8080/user/1?_format=hal_json"
                        },
                        "type": {
                            "href": "http://192.168.120.2:8080/rest/type/user/user"
                        }
                    },
                    "uuid": [
                        {
                            "value": "8508423a-92a1-488c-8002-1084bb3f9f2a"
                        }
                    ]
                }
            ],
            "http://192.168.120.2:8080/rest/relation/node/downloads/uid": [
                {
                    "_links": {
                        "self": {
                            "href": "http://192.168.120.2:8080/user/1?_format=hal_json"
                        },
                        "type": {
                            "href": "http://192.168.120.2:8080/rest/type/user/user"
                        }
                    },
                    "uuid": [
                        {
                            "value": "8508423a-92a1-488c-8002-1084bb3f9f2a"
                        }
                    ],
                    "lang": "zh-hans"
                }
            ],
            "http://192.168.120.2:8080/rest/relation/node/downloads/field_free_download_link": [
                {
                    "_links": {
                        "self": {
                            "href": "http://192.168.120.2:8080/entity/paragraph/13303?_format=hal_json"
                        },
                        "type": {
                            "href": "http://192.168.120.2:8080/rest/type/paragraph/free_download_link"
                        }
                    },
                    "uuid": [
                        {
                            "value": "cd991d0b-fb7e-4a33-92ee-d0f07d3220d2"
                        }
                    ],
                    "target_revision_id": "19702"
                }
            ],
            "http://192.168.120.2:8080/rest/relation/node/downloads/field_paid_download_link": [
                {
                    "_links": {
                        "self": {
                            "href": "http://192.168.120.2:8080/entity/paragraph/13304?_format=hal_json"
                        },
                        "type": {
                            "href": "http://192.168.120.2:8080/rest/type/paragraph/paid_download_link"
                        }
                    },
                    "uuid": [
                        {
                            "value": "60ed8ec2-1e20-496f-a23e-19757d12e832"
                        }
                    ],
                    "target_revision_id": "19703"
                }
            ],
            "http://192.168.120.2:8080/rest/relation/node/downloads/field_tags": [
                {
                    "_links": {
                        "self": {
                            "href": "http://192.168.120.2:8080/tags/SM-G9910?_format=hal_json"
                        },
                        "type": {
                            "href": "http://192.168.120.2:8080/rest/type/taxonomy_term/tags"
                        }
                    },
                    "uuid": [
                        {
                            "value": "6168d94f-8def-4631-9a70-5f2c5e5f890a"
                        }
                    ],
                    "lang": "zh-hans"
                }
            ]
        },
        "status": [
            {
                "value": True,
                "lang": "zh-hans"
            }
        ],
        "title": [
            {
                "value": "CHC-G9910ZCU2AUH1_ALL",
                "lang": "zh-hans"
            }
        ],
        "created": [
            {
                "value": "2021-09-23T04:41:50+00:00",
                "lang": "zh-hans",
                "format": "Y-m-d\\TH:i:sP"
            }
        ],
        "changed": [
            {
                "value": "2021-09-28T19:48:36+00:00",
                "lang": "zh-hans",
                "format": "Y-m-d\\TH:i:sP"
            }
        ],
        "promote": [
            {
                "value": False,
                "lang": "zh-hans"
            }
        ],
        "sticky": [
            {
                "value": False,
                "lang": "zh-hans"
            }
        ],
        "default_langcode": [
            {
                "value": True,
                "lang": "zh-hans"
            }
        ],
        "metatag": [
            {
                "value": {
                    "title": "CHC-G9910ZCU2AUH1_ALL | ROM中国",
                    "canonical_url": "http://192.168.120.2:8080/node/13857",
                    "description": "G9910ZCU2AUH1 五件套",
                    "keywords": "CHC-G9910ZCU2AUH1_ALL"
                }
            }
        ],
        "path": [
            {
                "alias": None,
                "pid": None,
                "langcode": "zh-hans",
                "lang": "zh-hans"
            }
        ],
        "body": [
            {
                "value": "<p>G9910ZCU2AUH1 五件套</p>\r\n",
                "format": "full_html",
                "processed": "<p>G9910ZCU2AUH1 五件套</p>\n",
                "summary": "",
                "lang": "zh-hans"
            }
        ],
        "field_remark": [
            {
                "value": "解压密码: cnroms.com@f5995b912b"
            }
        ],
        "field_roles": [
            {
                "target_id": "premium"
            }
        ],
        "field_version": [
            {
                "value": "11"
            }
        ]
    }

def patch_node_response():
    return {
        "_links": {
            "self": {
                "href": "http://192.168.120.2:8080/node/13861?_format=hal_json"
            },
            "type": {
                "href": "http://192.168.120.2:8080/rest/type/node/downloads"
            },
            "http://192.168.120.2:8080/rest/relation/node/downloads/revision_uid": [
                {
                    "href": "http://192.168.120.2:8080/user/1?_format=hal_json"
                }
            ],
            "http://192.168.120.2:8080/rest/relation/node/downloads/uid": [
                {
                    "href": "http://192.168.120.2:8080/user/1?_format=hal_json",
                    "lang": "zh-hans"
                }
            ],
            "http://192.168.120.2:8080/rest/relation/node/downloads/field_paid_download_link": [
                {
                    "href": "http://192.168.120.2:8080/entity/paragraph/13311?_format=hal_json"
                }
            ],
            "http://192.168.120.2:8080/rest/relation/node/downloads/field_tags": [
                {
                    "href": "http://192.168.120.2:8080/tags/SM-A105F?_format=hal_json",
                    "lang": "zh-hans"
                }
            ]
        },
        "nid": [
            {
                "value": 13861
            }
        ],
        "uuid": [
            {
                "value": "7355d12c-65d9-4cab-b673-6a47fc66f59c"
            }
        ],
        "vid": [
            {
                "value": 19588
            }
        ],
        "langcode": [
            {
                "value": "zh-hans",
                "lang": "zh-hans"
            }
        ],
        "type": [
            {
                "target_id": "downloads"
            }
        ],
        "revision_timestamp": [
            {
                "value": "2021-09-29T20:01:50+00:00",
                "format": "Y-m-d\\TH:i:sP"
            }
        ],
        "_embedded": {
            "http://192.168.120.2:8080/rest/relation/node/downloads/revision_uid": [
                {
                    "_links": {
                        "self": {
                            "href": "http://192.168.120.2:8080/user/1?_format=hal_json"
                        },
                        "type": {
                            "href": "http://192.168.120.2:8080/rest/type/user/user"
                        }
                    },
                    "uuid": [
                        {
                            "value": "8508423a-92a1-488c-8002-1084bb3f9f2a"
                        }
                    ]
                }
            ],
            "http://192.168.120.2:8080/rest/relation/node/downloads/uid": [
                {
                    "_links": {
                        "self": {
                            "href": "http://192.168.120.2:8080/user/1?_format=hal_json"
                        },
                        "type": {
                            "href": "http://192.168.120.2:8080/rest/type/user/user"
                        }
                    },
                    "uuid": [
                        {
                            "value": "8508423a-92a1-488c-8002-1084bb3f9f2a"
                        }
                    ],
                    "lang": "zh-hans"
                }
            ],
            "http://192.168.120.2:8080/rest/relation/node/downloads/field_paid_download_link": [
                {
                    "_links": {
                        "self": {
                            "href": "http://192.168.120.2:8080/entity/paragraph/13311?_format=hal_json"
                        },
                        "type": {
                            "href": "http://192.168.120.2:8080/rest/type/paragraph/paid_download_link"
                        }
                    },
                    "uuid": [
                        {
                            "value": "9d72055a-efb5-4465-ac70-c06ff0af2cd5"
                        }
                    ],
                    "target_revision_id": "19705"
                }
            ],
            "http://192.168.120.2:8080/rest/relation/node/downloads/field_tags": [
                {
                    "_links": {
                        "self": {
                            "href": "http://192.168.120.2:8080/tags/SM-A105F?_format=hal_json"
                        },
                        "type": {
                            "href": "http://192.168.120.2:8080/rest/type/taxonomy_term/tags"
                        }
                    },
                    "uuid": [
                        {
                            "value": "d2565220-ae14-44fe-8f46-be3a7b0015b5"
                        }
                    ],
                    "lang": "zh-hans"
                }
            ]
        },
        "status": [
            {
                "value": True,
                "lang": "zh-hans"
            }
        ],
        "title": [
            {
                "value": "patched",
                "lang": "zh-hans"
            }
        ],
        "created": [
            {
                "value": "2021-09-28T18:01:21+00:00",
                "lang": "zh-hans",
                "format": "Y-m-d\\TH:i:sP"
            }
        ],
        "changed": [
            {
                "value": "2021-09-29T20:02:23+00:00",
                "lang": "zh-hans",
                "format": "Y-m-d\\TH:i:sP"
            }
        ],
        "promote": [
            {
                "value": False,
                "lang": "zh-hans"
            }
        ],
        "sticky": [
            {
                "value": False,
                "lang": "zh-hans"
            }
        ],
        "default_langcode": [
            {
                "value": True,
                "lang": "zh-hans"
            }
        ],
        "revision_translation_affected": [
            {
                "value": True,
                "lang": "zh-hans"
            }
        ],
        "metatag": [
            {
                "value": {
                    "title": "patched | ROM中国",
                    "canonical_url": "http://192.168.120.2:8080/node/13861",
                    "description": "G9910ZCU2AUH1 五件套",
                    "keywords": "patched"
                }
            }
        ],
        "path": [
            {
                "alias": None,
                "pid": None,
                "langcode": "zh-hans",
                "lang": "zh-hans"
            }
        ],
        "body": [
            {
                "value": "<p>G9910ZCU2AUH1 五件套</p>\r\n",
                "format": "full_html",
                "processed": "<p>G9910ZCU2AUH1 五件套</p>\n",
                "summary": "",
                "lang": "zh-hans"
            }
        ],
        "field_remark": [
            {
                "value": "解压密码: cnroms.com@f5995b912b"
            }
        ],
        "field_roles": [
            {
                "target_id": "premium"
            }
        ],
        "field_version": [
            {
                "value": "11"
            }
        ]
    }

