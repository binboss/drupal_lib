"""..."""
from dataclasses import dataclass
from typing import List
from drupal_lib.types import *

@dataclass
class GetEntityResponse(BaseEntity):
    """..."""
    id: List[FieldValue]
    uuid: List[FieldValue]
    revision_id: List[FieldValue]
    created: List[FieldTimeWithLang]
    langcode: List[FieldValueWithLang]
    revision_translation_affected: List[FieldValueWithLang]