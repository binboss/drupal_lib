"""..."""
from dataclasses import dataclass, field
from typing import List
from drupal_lib.types import *

@dataclass
class GetTermResponse(BaseTerm):
    """..."""
    _embedded: field(default_factory=dict)
    tid: List[FieldValue]
    changed: List[FieldTimeWithLang]
    langcode: List[FieldValueWithLang]
    path: List[FieldPathWithLang]
    revision_id: List[FieldValue]
    revision_translation_affected: List[FieldValueWithLang]
    status: List[FieldValueWithLang]
    uuid: List[FieldValue]
    vid: List[FieldEntityTargetID]
    weight: List[FieldValue]