"""..."""
from dataclasses import dataclass, field
from typing import List
from drupal_lib.types import *

@dataclass
class GetNodeResponse(BaseNode):
    """..."""
    nid: List[FieldValue]
    uuid: List[FieldValue]
    vid: List[FieldValue]
    created: List[FieldTimeWithLang]
    changed: List[FieldTimeWithLang]
    _embedded: field(default_factory=dict)
    revision_timestamp: List[FieldTime]
    revision_translation_affected: List[FieldValueWithLang]
    langcode: List[FieldValueWithLang]
    path: List[FieldPathWithLang]