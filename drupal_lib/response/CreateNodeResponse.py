"""..."""
from dataclasses import dataclass, field
from typing import List

from drupal_lib.types import *


@dataclass
class CreateNodeResponse(BaseNode):
    """..."""
    nid: List[FieldValue]
    uuid: List[FieldValue]
    vid: List[FieldValue]
    created: List[FieldTimeWithLang]
    changed: List[FieldTimeWithLang]
    langcode: List[FieldValueWithLang]
    revision_timestamp: List[FieldTime]
    _embedded: field(default_factory=dict)
    revision_translation_affected: List[FieldValueWithLang]
    path: List[FieldPath]