from dataclasses import dataclass
from typing import List

from drupal_lib import DrupalLib
from drupal_lib.core import Auth
from drupal_lib.core.Config import *
from drupal_lib.request import *
from drupal_lib.response import *
from drupal_lib.types import *

@dataclass
class GetDownloadLinkResponse(NoneRefersDefault, GetEntityParagraphResponse):
    """..."""
    parent_id: List[FieldValue]
    parent_type: List[FieldValue]
    parent_field_name: List[FieldValue]
    field_pg_download_link: List[FieldLink]
    field_pg_download_password: List[FieldValue] = DefaultVal([])

    def __post_init__(self):
        NoneRefersDefault.__post_init__(self)
        GetEntityParagraphResponse.__post_init__(self)

auth = Auth(
    client_id='968d5f35-4fb5-4364-adae-8106cf1c804b',
    client_secret='511526BE9BD9EFB2B4B53C6EC85E3',
    # access_token='eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijc1YjliOWMyYTFkMzg1NTkwYzNlZjlhN2IzYmE1NDBhYjAwYzAwN2Y0NDFhZjhhN2Q2YWJhMGY4ZWZiZWY1MjU5MDdkZjhmNTkzNzM4YmNjIn0.eyJhdWQiOiI5NjhkNWYzNS00ZmI1LTQzNjQtYWRhZS04MTA2Y2YxYzgwNGIiLCJqdGkiOiI3NWI5YjljMmExZDM4NTU5MGMzZWY5YTdiM2JhNTQwYWIwMGMwMDdmNDQxYWY4YTdkNmFiYTBmOGVmYmVmNTI1OTA3ZGY4ZjU5MzczOGJjYyIsImlhdCI6MTYzMzAyMzI2NiwibmJmIjoxNjMzMDIzMjY2LCJleHAiOjE2NDA3OTkyNjYsInN1YiI6IjEiLCJzY29wZXMiOlsiYXV0aGVudGljYXRlZCJdfQ.XIGiRsZJtb6TrTT0T9obc4GTDeYkoOcBSgrByLwtx9cLoQDuKkcCnb4IBQWGiYyJJD6a3vZDIF2Ffkn7kyBzN3MfCjb9BoX8XGZW16gSV5BVIGDTtuz6maMZElCfQzg0Nxg1Tz_Bvr7EDBW7wv2KTYG6_27gULPG6rXhETB68yxdQwsMTd3v-dRd-aCEvlopRy965kHtdt966qxbgzQipWooVu5TM44i2GyWqkAyErVZGbFKgP2U5Cvi29q7WiyadtpZGbHffVLu8XBJY6Q996IBT3TL8_EwkRP-Sf3wZVasZchwern94eNCgUqmgCa5fDW4HLiGqhV3C15qLLTPAhymyD99KCH6eatDVSjHy-0J7D-CBwr4DmRUqlxdSscI8dZUN0RkQebCKSCmDESZvQTKaYVmF8ucr-_aEHt2Rn9i_rPOZlHLFgNQnvIthB35dVCHDpPOr-C5jyAXssp2E4lqlc9W6J6YGmK4RHDaUfB_Mzp2auQcHzJK8Saha0yUIUVWdigZ8WIfKCNBhJ0ga77ARAMOLOTYDsGStfgCC8Q_TxyZaqBRy6VboSj6OZN172icSe9jidxPvxAnIGmqchGZLJz_aZYmeb_K26QEXO3zeZKmfp5bUAZLl_EkpylsHv9ygtCMuJlKwB31eDDsR8Mc9CpMx8BFY4uGKE13eaE'
    level='DEBUG'
)
drupal = DrupalLib(auth=auth)

entity: GetDownloadLinkResponse = drupal.get_entity('paragraph', 13316, GetDownloadLinkResponse)
r = entity.field_pg_download_link[0]
print(r)
