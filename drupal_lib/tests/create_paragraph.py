"""..."""
from dataclasses import dataclass
from typing import List, Union

from drupal_lib import DrupalLib
from drupal_lib.core import Auth
from drupal_lib.core.Config import *
from drupal_lib.request import *
from drupal_lib.response import *
from drupal_lib.types import *

@dataclass
class CreateDownloadLinkResponse(NoneRefersDefault, GetEntityParagraphResponse):
    """..."""
    parent_id: List[FieldValue]
    parent_type: List[FieldValue]
    parent_field_name: List[FieldValue]
    field_pg_download_link: List[FieldLink]
    field_pg_download_password: List[FieldValue] = DefaultVal([])

    def __post_init__(self):
        NoneRefersDefault.__post_init__(self)
        GetEntityParagraphResponse.__post_init__(self)

@dataclass
class CreateDownloadLinkRequest(CreateEntityParagraphRequest):
    """..."""
    parent_id: List[FieldValue]
    parent_type: List[FieldValue]
    parent_field_name: List[FieldValue]
    field_pg_download_link: List[FieldLink]
    field_pg_download_password: List[FieldValue] = DefaultVal([])


def create_download(paragraph_type: str, parent_id: List[FieldValue], parent_type: List[FieldValue], parent_field_name: List[FieldValue], download_link: List[FieldLink], download_password: List[FieldValue]) -> Union[Null, CreateDownloadLinkResponse]:
    """Create a download

    :param paragraph_type: The paragraph type, ex: paid_download_link
    :param parent_id: The entity id that this paragraph attachs to
    :param parent_type: The type of the entity
    :param parent_field_name: The field name of the entity
    :param download_link: For the field 'field_pg_download_link'
    :param download_password: For the field 'field_pg_download_password'
    :return: CreateDownloadLinkResponse

    :Example:
    >>> res = create_download('paid_download_link', [FieldValue(value='13861')], [FieldValue(value='node')], [FieldValue(value='field_paid_download_link')], [FieldLink(uri='http://example.com', title='exmaple', options=[])], [FieldValue(value='pass')])
    >>> print(res)
    """
    lang = 'zh-hans'
    body = CreateDownloadLinkRequest(
        _links={
            "type": {
                "href": f"{API_HOST}/rest/type/paragraph/{paragraph_type}"
            }
        },
        type=[
            FieldEntityTargetID(
                target_id=paragraph_type
            )
        ],
        status=[
            FieldValueWithLang(
                value=True,
                lang = lang
            )
        ],
        parent_id=parent_id,
        parent_type=parent_type,
        parent_field_name=parent_field_name,
        behavior_settings = [
            FieldValue(
                value=[]
            )
        ],
        default_langcode=[
            FieldValueWithLang(
                value=True,
                lang=lang
            )
        ],
        field_pg_download_link=download_link,
        field_pg_download_password=download_password
    )

    entity: CreateDownloadLinkResponse = drupal.create_entity('paragraph', body, CreateDownloadLinkResponse)
    return entity

if __name__ == '__main__':
    """..."""
    auth = Auth(
        client_id='968d5f35-4fb5-4364-adae-8106cf1c804b',
        client_secret='511526BE9BD9EFB2B4B53C6EC85E3',
        # access_token='eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijc1YjliOWMyYTFkMzg1NTkwYzNlZjlhN2IzYmE1NDBhYjAwYzAwN2Y0NDFhZjhhN2Q2YWJhMGY4ZWZiZWY1MjU5MDdkZjhmNTkzNzM4YmNjIn0.eyJhdWQiOiI5NjhkNWYzNS00ZmI1LTQzNjQtYWRhZS04MTA2Y2YxYzgwNGIiLCJqdGkiOiI3NWI5YjljMmExZDM4NTU5MGMzZWY5YTdiM2JhNTQwYWIwMGMwMDdmNDQxYWY4YTdkNmFiYTBmOGVmYmVmNTI1OTA3ZGY4ZjU5MzczOGJjYyIsImlhdCI6MTYzMzAyMzI2NiwibmJmIjoxNjMzMDIzMjY2LCJleHAiOjE2NDA3OTkyNjYsInN1YiI6IjEiLCJzY29wZXMiOlsiYXV0aGVudGljYXRlZCJdfQ.XIGiRsZJtb6TrTT0T9obc4GTDeYkoOcBSgrByLwtx9cLoQDuKkcCnb4IBQWGiYyJJD6a3vZDIF2Ffkn7kyBzN3MfCjb9BoX8XGZW16gSV5BVIGDTtuz6maMZElCfQzg0Nxg1Tz_Bvr7EDBW7wv2KTYG6_27gULPG6rXhETB68yxdQwsMTd3v-dRd-aCEvlopRy965kHtdt966qxbgzQipWooVu5TM44i2GyWqkAyErVZGbFKgP2U5Cvi29q7WiyadtpZGbHffVLu8XBJY6Q996IBT3TL8_EwkRP-Sf3wZVasZchwern94eNCgUqmgCa5fDW4HLiGqhV3C15qLLTPAhymyD99KCH6eatDVSjHy-0J7D-CBwr4DmRUqlxdSscI8dZUN0RkQebCKSCmDESZvQTKaYVmF8ucr-_aEHt2Rn9i_rPOZlHLFgNQnvIthB35dVCHDpPOr-C5jyAXssp2E4lqlc9W6J6YGmK4RHDaUfB_Mzp2auQcHzJK8Saha0yUIUVWdigZ8WIfKCNBhJ0ga77ARAMOLOTYDsGStfgCC8Q_TxyZaqBRy6VboSj6OZN172icSe9jidxPvxAnIGmqchGZLJz_aZYmeb_K26QEXO3zeZKmfp5bUAZLl_EkpylsHv9ygtCMuJlKwB31eDDsR8Mc9CpMx8BFY4uGKE13eaE'
        level='DEBUG'
    )
    drupal = DrupalLib(auth=auth)
    entity = create_download(
        paragraph_type='free_download_link',
        parent_id=[FieldValue(value='13861')],
        parent_type=[FieldValue(value='node')],
        parent_field_name=[FieldValue(value='field_free_download_link')],
        download_link=[FieldLink(uri='https://pan.baidu.com/s/1E5C-bhANKKaX4zdYMKkxGg', title='百度云', options=[])],
        download_password=[FieldValue(value='91ht')])
    if entity:
        # l = entity.field_pg_download_link[0]
        l = FieldLink(uri='https://pan.baidu.com/s/1E5C-bhANKKaX4zdYMKkxGg', title='百度云', options=[])
        print(entity)