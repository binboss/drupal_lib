"""..."""
from dataclasses import dataclass
from typing import List, Union, Generic

from drupal_lib import DrupalLib
from drupal_lib.core import Auth
from drupal_lib.core.Config import *
from drupal_lib.request import *
from drupal_lib.response import *
from drupal_lib.types import *
from drupal_lib.types.DataClass import DataType


@dataclass
class MyGetTermFirmwaresSamsungResponse(GetTermResponse):
    """..."""
    metatag: List
    field_device_model: List[FieldValue]
    revision_created: List[FieldTime] = None


@dataclass
class MyCreateTermFirmwaresSamsungResponse(CreateTermResponse):
    """..."""
    metatag: List
    field_device_model: List[FieldValue]


@dataclass
class MyCreateTermFirmwaresSamsungRequest(CreateTermRequest):
    """..."""
    field_device_model: List[FieldValue] = None


def get_term(tid: int, response_type: Generic[DataType] = GetTermResponse) -> GetTermResponse:
    """Get a term

    :param tid: The term id
    :param response_type: The response type, default is GetTermResponse
    :return: GetTermResponse if response_type is None, otherwise the response_type

    :Example:
    >>> term = get_term(18584, MyGetTermResponse)
    >>> print(term)
    """
    term = drupal.get_term(tid, response_type)
    return term


def patch_term(tid: int, term_type: Generic[DataType] = None, response_type: Generic[DataType] = None, **kwargs) -> Union[Null, CreateTermResponse]:
    """Patch a term

    :param tid: The term id
    :param term_type: The term type matchs the tid, use to parse the exist term as the default is GetTermResponse
    :param response_type: The response type, default is CreateTermResponse
    :return: CreateTermResponse if response_type is None, otherwise the response_type

    :Example:
    >>> term = patch_term(18584, MyGetTermFirmwaresSamsungResponse, MyCreateTermFirmwaresSamsungResponse)
    >>> print(term)
    """
    if term_type:
        term = get_term(tid, term_type)
    else:
        term = get_term(tid)

    body = {
        "_links": term._links,
        # "name": [
        #     {
        #         "value": "test with patch"
        #     }
        # ],
        **kwargs
    }

    if response_type is None:
        response_type = CreateTermResponse
    term = drupal.patch_term(tid, body, response_type)

    return term


def create_term(taxonomy: str, name: List[FieldValueWithLang], parent: str = None,  parent_uuid: str = None, response_type: Generic[DataType] = None, **kwargs) -> Union[Null, CreateTermResponse]:
    """Create a term

    :param taxonomy: The taxonomy name, ex: tags
    :param parent: The parent that this term belongs to
    :param parent_uuid: The parent uuid
    :param kwargs: Other fields
    :return: CreateTermResponse if response_type is None, otherwise the response_type

    :Example:
    >>> term = create_term(
    >>>     taxonomy='firmwares',
    >>>     name=[
    >>>         FieldValueWithLang(
    >>>             value="test",
    >>>             lang="zh-hans"
    >>>         )
    >>>     ],
    >>>     parent='samsung',
    >>>     parent_uuid='f1ac4e40-1119-4425-ad2b-2ce1c8dffc62',
    >>>     field_device_model=[
    >>>         FieldValue(
    >>>             value='Samsung Galaxy S10'
    >>>         ),
    >>>     ]
    >>> )
    >>> print(term)
    """
    if not any((parent, parent_uuid)) is None:
        embedded = {
            f"{API_HOST}/rest/relation/taxonomy_term/{taxonomy}/parent": [
                {
                    "_links": {
                        "self": {
                            "href": f"{API_HOST}/{taxonomy}/{parent}?_format=hal_json"
                        },
                        "type": {
                            "href": f"{API_HOST}/rest/type/taxonomy_term/{taxonomy}"
                        }
                    },
                    "uuid": [
                        {
                            "value": parent_uuid
                        }
                    ]
                }
            ]
        }

    else:
        embedded = None

    if response_type is None:
        response_type = CreateTermResponse

    body = response_type(
        _links={
            "type": {
                "href": f"{API_HOST}/rest/type/taxonomy_term/{taxonomy}"
            }
        },
        _embedded=embedded,
        vid=[
            FieldEntityTargetID(
                target_id=taxonomy
            )
        ],
        name=name,
        default_langcode=[
            FieldValueWithLang(
                value=True,
                lang="zh-hans"
            )
        ],
        **kwargs
    )

    term = drupal.create_term(body, response_type)

    return term


def delete_term(tid: int) -> None:
    """..."""
    drupal.delete_term(tid, DeleteTermResponse)


def create_device_term(taxonomy: str, name: List[FieldValueWithLang], parent: str = None,  parent_uuid: str = None, field_device_model: List[FieldValue] = None) -> Union[Null, MyCreateTermFirmwaresSamsungResponse]:
    """Create a device term

    :param taxonomy: The taxonomy name, ex: tags
    :param parent: The parent that this term belongs to
    :param parent_uuid: The parent uuid
    :param field_device_model: For the field 'field_device_model'
    :return: MyCreateTermFirmwaresSamsungResponse

    :Example:
    >>> term = create_term(
    >>>     taxonomy='firmwares',
    >>>     name=[
    >>>         FieldValueWithLang(
    >>>             value="test",
    >>>             lang="zh-hans"
    >>>         )
    >>>     ],
    >>>     parent='samsung',
    >>>     parent_uuid='f1ac4e40-1119-4425-ad2b-2ce1c8dffc62',
    >>>     field_device_model=[
    >>>         FieldValue(
    >>>             value='Samsung Galaxy S10'
    >>>         ),
    >>>     ]
    >>> )
    >>> print(term)
    """
    term: MyCreateTermFirmwaresSamsungResponse = create_term(
        taxonomy, name, parent, parent_uuid, response_type=MyCreateTermFirmwaresSamsungResponse, field_device_model=field_device_model)

    return term


if __name__ == '__main__':
    """..."""
    auth = Auth(
        client_id='968d5f35-4fb5-4364-adae-8106cf1c804b',
        client_secret='511526BE9BD9EFB2B4B53C6EC85E3',
        # access_token='eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijc1YjliOWMyYTFkMzg1NTkwYzNlZjlhN2IzYmE1NDBhYjAwYzAwN2Y0NDFhZjhhN2Q2YWJhMGY4ZWZiZWY1MjU5MDdkZjhmNTkzNzM4YmNjIn0.eyJhdWQiOiI5NjhkNWYzNS00ZmI1LTQzNjQtYWRhZS04MTA2Y2YxYzgwNGIiLCJqdGkiOiI3NWI5YjljMmExZDM4NTU5MGMzZWY5YTdiM2JhNTQwYWIwMGMwMDdmNDQxYWY4YTdkNmFiYTBmOGVmYmVmNTI1OTA3ZGY4ZjU5MzczOGJjYyIsImlhdCI6MTYzMzAyMzI2NiwibmJmIjoxNjMzMDIzMjY2LCJleHAiOjE2NDA3OTkyNjYsInN1YiI6IjEiLCJzY29wZXMiOlsiYXV0aGVudGljYXRlZCJdfQ.XIGiRsZJtb6TrTT0T9obc4GTDeYkoOcBSgrByLwtx9cLoQDuKkcCnb4IBQWGiYyJJD6a3vZDIF2Ffkn7kyBzN3MfCjb9BoX8XGZW16gSV5BVIGDTtuz6maMZElCfQzg0Nxg1Tz_Bvr7EDBW7wv2KTYG6_27gULPG6rXhETB68yxdQwsMTd3v-dRd-aCEvlopRy965kHtdt966qxbgzQipWooVu5TM44i2GyWqkAyErVZGbFKgP2U5Cvi29q7WiyadtpZGbHffVLu8XBJY6Q996IBT3TL8_EwkRP-Sf3wZVasZchwern94eNCgUqmgCa5fDW4HLiGqhV3C15qLLTPAhymyD99KCH6eatDVSjHy-0J7D-CBwr4DmRUqlxdSscI8dZUN0RkQebCKSCmDESZvQTKaYVmF8ucr-_aEHt2Rn9i_rPOZlHLFgNQnvIthB35dVCHDpPOr-C5jyAXssp2E4lqlc9W6J6YGmK4RHDaUfB_Mzp2auQcHzJK8Saha0yUIUVWdigZ8WIfKCNBhJ0ga77ARAMOLOTYDsGStfgCC8Q_TxyZaqBRy6VboSj6OZN172icSe9jidxPvxAnIGmqchGZLJz_aZYmeb_K26QEXO3zeZKmfp5bUAZLl_EkpylsHv9ygtCMuJlKwB31eDDsR8Mc9CpMx8BFY4uGKE13eaE'
        level='DEBUG'
    )
    drupal = DrupalLib(auth=auth)

    # Test Get Term
    term: MyGetTermFirmwaresSamsungResponse = get_term(18397, MyGetTermFirmwaresSamsungResponse)

    # Test Create Term
    # term = create_device_term(
    #     taxonomy='firmwares',
    #     name=[
    #         FieldValueWithLang(
    #             value="test",
    #             lang="zh-hans"
    #         )
    #     ],
    #     parent='samsung',
    #     parent_uuid='f1ac4e40-1119-4425-ad2b-2ce1c8dffc62',
    #     field_device_model=[
    #         FieldValue(
    #             value='Samsung Galaxy S10'
    #         ),
    #     ]
    # )

    # Test Delete Term
    # delete_term(18584)

    print(term)
    # tid = term.tid[0].value
    # print(tid)
    # print(term.name[0].value)
    # term = patch_term(tid)
    # print(term.name[0].value)
    # delete_term(tid)


    # Test patch
    # body = {
    #     "name": [
    #         {
    #             "value": "test with patch",
    #         }
    #     ]
    # }
    # term = patch_term(18584, MyGetTermFirmwaresSamsungResponse,
    #                   MyCreateTermFirmwaresSamsungResponse, **body)
    # print(term)
