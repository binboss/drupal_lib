import dataclasses
from dataclasses import dataclass, fields, field
from typing import Any, List
import json

from websockets import Data

@dataclass
class DefaultVal:
    val: Any


@dataclass
class NoneRefersDefault:
    def __post_init__(self):
        for field in fields(self):

            # if a field of this data class defines a default value of type
            # `DefaultVal`, then use its value in case the field after 
            # initialization has either not changed or is None.
            if isinstance(field.default, DefaultVal):
                field_val = getattr(self, field.name)
                if isinstance(field_val, DefaultVal) or field_val is None:
                    setattr(self, field.name, field.default.val)


@dataclass
class Base:
    a: List[str]
    b: str
    c: str

@dataclass
class Specs3(NoneRefersDefault, Base):
    a: str
    b: str = DefaultVal([])
    c: str = DefaultVal('Charlie')

@dataclass
class Specs4(Base):
    d: str

@dataclass
class Pizza():
    ingredients: List = field(default_factory=lambda: ['dow', 'tomatoes'])


r3 = Specs3('Apple')
print(r3)


from drupal_lib.types import *



@dataclass
class GetEntityParagraphResponse(DataClass):
    """..."""
    _links: field(default_factory=dict)
    default_langcode: List[FieldValueWithLang]
    id: List[FieldValue]
    uuid: List[FieldValue]
    revision_id: List[FieldValue]
    created: List[FieldValue]
    langcode: List[FieldValue]
    revision_translation_affected: List[FieldValueWithLang]
    status: List[FieldValueWithLang]
    type: List[FieldEntityTargetID]
    behavior_settings: List[FieldValue]

@dataclass
class GetEntityParagraphResponseT(DataClass):
    """..."""
    id: FieldValue

@dataclass
class GetPaidDownloadLinkResponse(GetEntityParagraphResponseT):
    """..."""
    parent_id: List[FieldValue]
    # parent_type: List[FieldValue]
    # parent_field_name: List[FieldValue]
    # field_pg_download_link: List[FieldLink]
    # field_pg_download_password: List[FieldValue]

data = '{"_links":{"self":{"href":"http:\\/\\/192.168.120.2:8080\\/entity\\/paragraph\\/13312?_format=hal_json"},"type":{"href":"http:\\/\\/192.168.120.2:8080\\/rest\\/type\\/paragraph\\/paid_download_link"}},"id":[{"value":13312}],"uuid":[{"value":"1000a11b-8efc-4126-bab1-1fafc601f9d0"}],"revision_id":[{"value":19706}],"langcode":[{"value":"zh-hans","lang":"zh-hans"}],"type":[{"target_id":"paid_download_link"}],"status":[{"value":true,"lang":"zh-hans"}],"created":[{"value":"2021-09-23T04:42:03+00:00","lang":"zh-hans","format":"Y-m-d\\\\TH:i:sP"}],"parent_id":[{"value":"13861"}],"parent_type":[{"value":"node"}],"parent_field_name":[{"value":"field_paid_download_link"}],"behavior_settings":[{"value":[]}],"default_langcode":[{"value":true,"lang":"zh-hans"}],"revision_translation_affected":[{"value":true,"lang":"zh-hans"}],"field_pg_download_link":[{"uri":"https:\\/\\/pan.baidu.com\\/s\\/1E5C-bhANKKaX4zdYMKkxGg","title":"\\u767e\\u5ea6\\u4e91","options":[]}],"field_pg_download_password":[{"value":"91ht"}]}'

data = {
    'parent_id': [
        {
            'value': 123
        }
    ],
    'id': {
        'value': 456
    }
}
data = json.dumps(data)
r4 = GetPaidDownloadLinkResponse(**json.loads(data))
# from typing import get_type_hints
# print(get_type_hints(r4))
print(r4)
# print(r4.id.value)