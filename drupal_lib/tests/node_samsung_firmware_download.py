import sys
from dataclasses import dataclass, field, fields
from typing import Dict, List

from drupal_lib import DrupalLib
from drupal_lib.core import Auth
from drupal_lib.core.Config import *
from drupal_lib.request import *
from drupal_lib.response import *
from drupal_lib.types.DefaultVal import DefaultVal
from drupal_lib.types.FieldValue import FieldValue
from drupal_lib.types.NodeFieldBody import NodeFieldBody
from drupal_lib.types.NodeFieldBodyWithLang import NodeFieldBodyWithLang
from drupal_lib.types.FieldEntityTargetID import FieldEntityTargetID

@dataclass
class GetNodeDownloadsResponse(GetNodeResponse):
    """..."""
    metatag: field(default_factory=list)
    body: List[NodeFieldBody] = field(default=None, repr=False)

@dataclass
class GetNodeSamsungFirmwareDownloadResponse(GetNodeResponse):
    """..."""
    metatag: field(default_factory=list)
    comment: field(default_factory=list)
    field_changelog: field(default_factory=list)
    field_combination: field(default_factory=list)
    field_samsung_ap: field(default_factory=list)
    field_samsung_build_date: field(default_factory=list)
    field_samsung_changelist: field(default_factory=list)
    field_samsung_csc: field(default_factory=list)
    field_samsung_modem: field(default_factory=list)
    field_yoast_seo: field(default_factory=list)
    body: List[NodeFieldBodyWithLang] = field(default=None, repr=False)

@dataclass
class CreateNodeSamsungFirmwareDownloadResponse(CreateNodeResponse):
    """..."""
    metatag: field(default_factory=list)
    comment: field(default_factory=list)
    field_changelog: field(default_factory=list)
    field_combination: field(default_factory=list)
    field_samsung_ap: field(default_factory=list)
    field_samsung_build_date: field(default_factory=list)
    field_samsung_changelist: field(default_factory=list)
    field_samsung_csc: field(default_factory=list)
    field_samsung_modem: field(default_factory=list)
    field_yoast_seo: field(default_factory=list)


def get_node(node_id: int) -> GetNodeSamsungFirmwareDownloadResponse:
    node: GetNodeSamsungFirmwareDownloadResponse = drupal.get_node(node_id, GetNodeSamsungFirmwareDownloadResponse)
    return node


if __name__ == '__main__':

    auth = Auth(
        client_id='968d5f35-4fb5-4364-adae-8106cf1c804b',
        client_secret='511526BE9BD9EFB2B4B53C6EC85E3',
        # access_token='eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijc1YjliOWMyYTFkMzg1NTkwYzNlZjlhN2IzYmE1NDBhYjAwYzAwN2Y0NDFhZjhhN2Q2YWJhMGY4ZWZiZWY1MjU5MDdkZjhmNTkzNzM4YmNjIn0.eyJhdWQiOiI5NjhkNWYzNS00ZmI1LTQzNjQtYWRhZS04MTA2Y2YxYzgwNGIiLCJqdGkiOiI3NWI5YjljMmExZDM4NTU5MGMzZWY5YTdiM2JhNTQwYWIwMGMwMDdmNDQxYWY4YTdkNmFiYTBmOGVmYmVmNTI1OTA3ZGY4ZjU5MzczOGJjYyIsImlhdCI6MTYzMzAyMzI2NiwibmJmIjoxNjMzMDIzMjY2LCJleHAiOjE2NDA3OTkyNjYsInN1YiI6IjEiLCJzY29wZXMiOlsiYXV0aGVudGljYXRlZCJdfQ.XIGiRsZJtb6TrTT0T9obc4GTDeYkoOcBSgrByLwtx9cLoQDuKkcCnb4IBQWGiYyJJD6a3vZDIF2Ffkn7kyBzN3MfCjb9BoX8XGZW16gSV5BVIGDTtuz6maMZElCfQzg0Nxg1Tz_Bvr7EDBW7wv2KTYG6_27gULPG6rXhETB68yxdQwsMTd3v-dRd-aCEvlopRy965kHtdt966qxbgzQipWooVu5TM44i2GyWqkAyErVZGbFKgP2U5Cvi29q7WiyadtpZGbHffVLu8XBJY6Q996IBT3TL8_EwkRP-Sf3wZVasZchwern94eNCgUqmgCa5fDW4HLiGqhV3C15qLLTPAhymyD99KCH6eatDVSjHy-0J7D-CBwr4DmRUqlxdSscI8dZUN0RkQebCKSCmDESZvQTKaYVmF8ucr-_aEHt2Rn9i_rPOZlHLFgNQnvIthB35dVCHDpPOr-C5jyAXssp2E4lqlc9W6J6YGmK4RHDaUfB_Mzp2auQcHzJK8Saha0yUIUVWdigZ8WIfKCNBhJ0ga77ARAMOLOTYDsGStfgCC8Q_TxyZaqBRy6VboSj6OZN172icSe9jidxPvxAnIGmqchGZLJz_aZYmeb_K26QEXO3zeZKmfp5bUAZLl_EkpylsHv9ygtCMuJlKwB31eDDsR8Mc9CpMx8BFY4uGKE13eaE'
        level='DEBUG'
    )
    drupal = DrupalLib(auth=auth)

    # node: GetNodeDownloadsResponse = drupal.get_node(11331, GetNodeDownloadsResponse)
    node = get_node(13859)
    print(node.title)

    sys.exit()

node_type = 'downloads'
title = "2022-02-17 16:02"
body = "this is body"
node_body = CreateNodeDownloadsRequest(
    _links = {
        "type": {
            "href": f"{API_HOST}/rest/type/node/{node_type}"
        }
    },
    _embedded = {
        f"http://{API_HOST}/rest/relation/node/downloads/field_paid_download_link": [
            {
                "_links": {
                    "self": {
                        "href": ""
                    },
                    "type": {
                        "href": "http://192.168.120.2:8080/rest/type/paragraph/paid_download_link"
                    }
                },
                "uuid": [
                    {
                        "value": "9d72055a-efb5-4465-ac70-c06ff0af2cd5"
                    }
                ],
                "target_revision_id": 19705
            }
        ]
    },
    type = [
        {
            "target_id": node_type
        }
    ],
    status = [
        {
            "value": True,
            "lang": "zh-hans"
        }
    ],
    title = [
        {
            "value": title,
            "lang": "zh-hans"
        }
    ],
    body = [
        {
            "value": body,
            # "format": 'full_html',
            "summary": "",
            "lang": "zh-hans"
        }
    ],
    field_remark = [
        {
            "value": "解压密码: cnroms.com@f5995b912b"
        }
    ],
    field_roles = [
        {
            "target_id": "premium"
        }
    ],
    field_version = [
        {
            "value": "11"
        }
    ],
    field_tags = [
        {
            "target_id": 18397
        }
    ]
)
# node: CreateNodeSamsungFirmwareDownloadResponse = drupal.create_node(node_body, CreateNodeSamsungFirmwareDownloadResponse)
# node: CreateNodeDownloadsResponse = drupal.create_node(node_body, CreateNodeDownloadsResponse)
print(node)