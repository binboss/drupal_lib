import json
from dataclasses import dataclass, field, fields
from typing import Dict, List

from drupal_lib import DrupalLib
from drupal_lib.core import Auth
from drupal_lib.core.Config import *
from drupal_lib.request import *
from drupal_lib.response import *
from drupal_lib.types.DefaultVal import DefaultVal
from drupal_lib.types.FieldValue import FieldValue
from drupal_lib.types.NodeFieldBody import NodeFieldBody
from drupal_lib.types.NodeFieldBodyWithLang import NodeFieldBodyWithLang
from drupal_lib.types.FieldEntityTargetID import FieldEntityTargetID

@dataclass
class GetNodeDownloadsResponse(GetNodeResponse):
    """..."""
    metatag: field(default_factory=list) = DefaultVal([])
    field_remark: List[FieldValue] = DefaultVal([])
    field_roles: List[FieldEntityTargetID] = DefaultVal([])
    field_version: List[FieldValue] = DefaultVal([])
    body: List[NodeFieldBodyWithLang] = field(default=None, repr=False)


@dataclass
class CreateNodeDownloadsResponse(CreateNodeResponse):
    """..."""
    metatag: field(default_factory=list)
    field_remark: List[FieldValue]
    field_roles: List[FieldEntityTargetID]
    field_version: List[FieldValue]

@dataclass
class CreateNodeDownloadsRequest(CreateNodeRequest):
    """..."""
    _embedded: List = field(default_factory=list)
    field_remark: List = field(default_factory=list)
    field_roles: List = field(default_factory=list)
    field_version: List = field(default_factory=list)
    field_tags: List = field(default_factory=list)


def get_download(node_id: int) -> GetNodeDownloadsResponse:
    node: GetNodeDownloadsResponse = drupal.get_node(node_id, GetNodeDownloadsResponse)
    return node

if __name__ == '__main__':

    auth = Auth(
        client_id='968d5f35-4fb5-4364-adae-8106cf1c804b',
        client_secret='511526BE9BD9EFB2B4B53C6EC85E3',
        # access_token='eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijc1YjliOWMyYTFkMzg1NTkwYzNlZjlhN2IzYmE1NDBhYjAwYzAwN2Y0NDFhZjhhN2Q2YWJhMGY4ZWZiZWY1MjU5MDdkZjhmNTkzNzM4YmNjIn0.eyJhdWQiOiI5NjhkNWYzNS00ZmI1LTQzNjQtYWRhZS04MTA2Y2YxYzgwNGIiLCJqdGkiOiI3NWI5YjljMmExZDM4NTU5MGMzZWY5YTdiM2JhNTQwYWIwMGMwMDdmNDQxYWY4YTdkNmFiYTBmOGVmYmVmNTI1OTA3ZGY4ZjU5MzczOGJjYyIsImlhdCI6MTYzMzAyMzI2NiwibmJmIjoxNjMzMDIzMjY2LCJleHAiOjE2NDA3OTkyNjYsInN1YiI6IjEiLCJzY29wZXMiOlsiYXV0aGVudGljYXRlZCJdfQ.XIGiRsZJtb6TrTT0T9obc4GTDeYkoOcBSgrByLwtx9cLoQDuKkcCnb4IBQWGiYyJJD6a3vZDIF2Ffkn7kyBzN3MfCjb9BoX8XGZW16gSV5BVIGDTtuz6maMZElCfQzg0Nxg1Tz_Bvr7EDBW7wv2KTYG6_27gULPG6rXhETB68yxdQwsMTd3v-dRd-aCEvlopRy965kHtdt966qxbgzQipWooVu5TM44i2GyWqkAyErVZGbFKgP2U5Cvi29q7WiyadtpZGbHffVLu8XBJY6Q996IBT3TL8_EwkRP-Sf3wZVasZchwern94eNCgUqmgCa5fDW4HLiGqhV3C15qLLTPAhymyD99KCH6eatDVSjHy-0J7D-CBwr4DmRUqlxdSscI8dZUN0RkQebCKSCmDESZvQTKaYVmF8ucr-_aEHt2Rn9i_rPOZlHLFgNQnvIthB35dVCHDpPOr-C5jyAXssp2E4lqlc9W6J6YGmK4RHDaUfB_Mzp2auQcHzJK8Saha0yUIUVWdigZ8WIfKCNBhJ0ga77ARAMOLOTYDsGStfgCC8Q_TxyZaqBRy6VboSj6OZN172icSe9jidxPvxAnIGmqchGZLJz_aZYmeb_K26QEXO3zeZKmfp5bUAZLl_EkpylsHv9ygtCMuJlKwB31eDDsR8Mc9CpMx8BFY4uGKE13eaE'
        level='DEBUG'
    )
    drupal = DrupalLib(auth=auth)
    # node: GetNodeDownloadsResponse = drupal.get_node(11331, GetNodeDownloadsResponse)
    # node = get_download(13861)
    node = get_download(11331)
    # print(json.dumps(node.__dict__, ensure_ascii=False, default=lambda o: o.__dict__))
    print(node.uuid)