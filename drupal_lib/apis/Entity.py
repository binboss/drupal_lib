from typing import Generic
from drupal_lib.core import *
from drupal_lib.request import *
from drupal_lib.response import *
from drupal_lib.types import *
from drupal_lib.types.DataClass import DataType
from drupal_lib.types.Enum import *


class Entity(Core):
    """..."""

    def get_entity(self,
                 entity_type: str, entity_id: str, response_type: Generic[DataType]) -> BaseEntity:
        """获取文件信息, 其他类中可能会用到, 所以放到基类中"""
        body = GetEntityRequest(
            entity_id=entity_id
        )

        return self._core_get_entity(entity_type, body, response_type)

    def create_entity(self,
                 entity_type: str, entity_body: CreateEntityRequest, response_type: Generic[DataType]) -> CreateEntityResponse:
        """获取文件信息, 其他类中可能会用到, 所以放到基类中"""

        return self._core_create_entity(entity_type, entity_body, response_type)