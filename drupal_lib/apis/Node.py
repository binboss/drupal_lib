from typing import Generic
from drupal_lib.core import *
from drupal_lib.request import *
from drupal_lib.response import *
from drupal_lib.types import *
from drupal_lib.types.DataClass import DataType
from drupal_lib.types.Enum import *


class Node(Core):
    """..."""

    def get_node(self,
                 node_id: str, response_type: Generic[DataType]) -> BaseNode:
        """获取文件信息, 其他类中可能会用到, 所以放到基类中"""
        body = GetNodeRequest(
            node_id=node_id
        )

        return self._core_get_node(body, response_type)

    def create_node(self,
                 node_body: CreateNodeRequest, response_type: Generic[DataType]) -> CreateNodeResponse:
        """获取文件信息, 其他类中可能会用到, 所以放到基类中"""

        return self._core_create_node(node_body, response_type)