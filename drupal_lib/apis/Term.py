from typing import Dict, Generic
from drupal_lib.core import *
from drupal_lib.request import *
from drupal_lib.response import *
from drupal_lib.types import *
from drupal_lib.types.DataClass import DataType
from drupal_lib.types.Enum import *


class Term(Core):
    """..."""

    def get_term(self,
                 tid: int, response_type: Generic[DataType]) -> GetTermResponse:
        """获取文件信息, 其他类中可能会用到, 所以放到基类中"""
        body = GetTermRequest(
            tid=tid
        )

        return self._core_get_term(body, response_type)

    def create_term(self,
                 term_body: CreateTermRequest, response_type: Generic[DataType]) -> CreateTermResponse:
        """获取文件信息, 其他类中可能会用到, 所以放到基类中"""

        return self._core_create_term(term_body, response_type)


    def patch_term(self,
                 tid: int, term_body: Dict, response_type: Generic[DataType]) -> CreateTermResponse:
        """获取文件信息, 其他类中可能会用到, 所以放到基类中"""

        return self._core_patch_term(tid, term_body, response_type)


    def delete_term(self,
                 tid: str, response_type: Generic[DataType]) -> DeleteTermResponse:
        """获取文件信息, 其他类中可能会用到, 所以放到基类中"""
        body = DeleteTermRequest(
            tid=tid
        )

        return self._core_delete_term(body, response_type)