"""..."""

from dataclasses import dataclass
from typing import Union

from drupal_lib.types.DataClass import DataClass


@dataclass
class GetTermRequest(DataClass):
    """..."""
    tid: Union[str, int]