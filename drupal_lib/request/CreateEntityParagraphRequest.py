"""..."""

from dataclasses import dataclass, field

from drupal_lib.types import *
from drupal_lib.types.Enum import *
from .CreateEntityRequest import CreateEntityRequest


@dataclass
class CreateEntityParagraphRequest(CreateEntityRequest):
    """..."""
    status: List[FieldValueWithLang]
    type: List[FieldEntityTargetID]
    behavior_settings: List[FieldValue]