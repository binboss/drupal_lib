"""..."""

from dataclasses import dataclass, field

from drupal_lib.types import *
from drupal_lib.types.Enum import *


@dataclass
class GetNodeRequest(DataClass):
    """..."""
    node_id: str