"""..."""

from dataclasses import dataclass
from typing import Union

from drupal_lib.types.DataClass import DataClass


@dataclass
class GetEntityRequest(DataClass):
    """..."""
    entity_id: Union[str, int]