"""..."""
from dataclasses import dataclass, field
from typing import List, Optional

from drupal_lib.types import *
from drupal_lib.types.Enum import *

@dataclass
class CreateTermRequest(NoneRefersDefault, BaseTerm):
    """..."""
    _embedded: field(default_factory=dict)
    vid: List[FieldEntityTargetID]
    langcode: List[FieldValueWithLang] = None
    path: Optional[List[FieldPathWithLang]] = None
    status: Optional[List[FieldValueWithLang]] = None
    weight: Optional[List[FieldValue]] = None