"""..."""

from dataclasses import dataclass
from typing import Union

from .GetTermRequest import GetTermRequest


@dataclass
class DeleteTermRequest(GetTermRequest):
    """..."""
    tid: Union[str, int]