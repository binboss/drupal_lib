"""..."""
from dataclasses import dataclass, field
from typing import List, Dict

from drupal_lib.types import *
from drupal_lib.types.Enum import *

@dataclass
class CreateEntityRequest(NoneRefersDefault, BaseEntity):
    """..."""