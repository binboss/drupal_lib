"""..."""
from dataclasses import dataclass, field
from typing import List, Dict

from drupal_lib.types import *
from drupal_lib.types.Enum import *

@dataclass
class CreateNodeRequest(NoneRefersDefault, BaseNode):
    """..."""
    status: List = DefaultVal([
        {
            "value": True,
            "lang": "zh-hans"
        }
    ])
    promote: List = DefaultVal([
        {
            "value": False,
            "lang": "zh-hans"
        }
    ])
    sticky: List = DefaultVal([
        {
            "value": False,
            "lang": "zh-hans"
        }
    ])
    default_langcode: List = DefaultVal([
        {
            "value":  True,
            "lang": "zh-hans"
        }
    ])
    body: List = DefaultVal([
        {
            "value": "",
            "format": "full_html",
            "summary": "",
            "lang": "zh-hans"
        }
    ])


    def __post_init__(self):
        NoneRefersDefault.__post_init__(self)
        BaseNode.__post_init__(self)