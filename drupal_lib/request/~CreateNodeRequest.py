"""..."""
from dataclasses import dataclass, field
from typing import List

from drupal_lib.types import *
from drupal_lib.types.Enum import *

@dataclass
class CreateNodeRequest(NoneRefersDefault, BaseNode):
    """..."""
    # status: List = DefaultVal([
    #     {
    #         "value": True,
    #         "lang": "zh-hans"
    #     }
    # ])
    # promote: List = DefaultVal([
    #     {
    #         "value": False,
    #         "lang": "zh-hans"
    #     }
    # ])
    # sticky: List = DefaultVal([
    #     {
    #         "value": False,
    #         "lang": "zh-hans"
    #     }
    # ])
    # default_langcode: List = DefaultVal([
    #     {
    #         "value":  True,
    #         "lang": "zh-hans"
    #     }
    # ])
    # body: List = DefaultVal([
    #     {
    #         "value": "",
    #         "format": "full_html",
    #         "summary": "",
    #         "lang": "zh-hans"
    #     }
    # ])

    status: List = field(default_factory=list)
    promote: List = field(default_factory=list)
    sticky: List = field(default_factory=list)
    default_langcode: List = field(default_factory=list)
    body: List = field(default_factory=list)

    def __post_init__(self):
        self.status = [{
            "value": True,
            "lang": "zh-hans"
        }]
        self.promote = [
            {
                "value": False,
                "lang": "zh-hans"
            }
        ]
        self.sticky = [
            {
                "value": False,
                "lang": "zh-hans"
            }
        ]
        self.default_langcode = [
            {
                "value":  True,
                "lang": "zh-hans"
            }
        ]
        self.body = [
            {
                "value": "",
                "format": "full_html",
                "summary": "",
                "lang": "zh-hans"
            }
        ]